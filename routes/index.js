var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/contacts', function(req, res, next) {
  res.render('contact', { title: 'COntact' });
});

router.get('/services', function(req, res, next) {
  let services  = ["PHP", "Angular", "React", "Bootstrap"];
  res.render('services', { title: 'Services', services: services });
});

router.get('/home', function(req, res, next) {
  res.render('index', { title: 'Home' });
});
module.exports = router;
